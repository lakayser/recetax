package com.example.recetax;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RecetasActivity extends AppCompatActivity {
    private TextView tvReceta, tvNombre;
    private EditText etBuscador;
    private Button btnBuscar, btnNutricion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recetas);

        this.tvReceta = (TextView) findViewById(R.id.tvReceta);
        this.tvNombre = (TextView) findViewById(R.id.tvNombre);
        this.etBuscador = (EditText) findViewById(R.id.etBuscador);
        this.btnBuscar = (Button) findViewById(R.id.btnBuscar);
        this.btnNutricion = (Button) findViewById(R.id.btnNutricion);


        this.btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String clave = etBuscador.getText().toString();

                String url = "https://api.edamam.com/search?q=" + clave + "&app_id=4a0e061c&app_key=a761efa4d7cbd94cfdbadaa69ea25a9d";
                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // llega respuesta del servidor
                                try {
                                    Log.i("Respuesta", response);
                                    JSONObject respuestaJSON = new JSONObject(response);

                                    JSONArray ingreJSON = respuestaJSON.getJSONArray("hits");
                                    JSONObject ingre2JSON = ingreJSON.getJSONObject(0);
                                    JSONObject ingre3JSON = ingre2JSON.getJSONObject("recipe");

                                    JSONArray ingre4JSON = ingre3JSON.getJSONArray("ingredientLines");
                                    String ingredientes = " ";

                                    String ingre5JSON = ingre3JSON.getString("label");
                                    String nombre = ingre5JSON;
                                    tvNombre.setText("Hoy vamos a preparar " + nombre);

                                    for (int i = 0; i < ingre4JSON.length(); i++) {
                                        ingredientes = ingredientes + "\n" + ingre4JSON.getString(i);
                                    }

                                    tvReceta.setText(ingredientes);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // Algo fallo
                            }
                        }
                );

                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);


            }
        });


        this.btnNutricion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String clave2 = etBuscador.getText().toString();

                Intent i = new Intent(RecetasActivity.this, NutricionActivity.class);
                i.putExtra("dato1", clave2);
                startActivity(i);

            }
        });

    }
}
