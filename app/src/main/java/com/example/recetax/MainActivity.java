package com.example.recetax;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private Button btnReceta, btnNutricion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.btnReceta = (Button)findViewById(R.id.btnReceta);
        this.btnNutricion = (Button)findViewById(R.id.btnNutricion);


        this.btnNutricion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this, BuscadorActivity.class));

            }
        });

        this.btnReceta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this, RecetasActivity.class));

            }
        });


    }
}
