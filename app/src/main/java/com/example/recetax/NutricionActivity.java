package com.example.recetax;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NutricionActivity extends AppCompatActivity {

    private TextView tvNutricion, tvNutricion1, tvNutricion2, tvNutricion3, tvNutricion4, tvNutricion5;
    private Button btnAtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nutricion);

        tvNutricion = (TextView) findViewById(R.id.tvNutricion);
        tvNutricion1 = (TextView) findViewById(R.id.tvNutricion1);
        tvNutricion2 = (TextView) findViewById(R.id.tvNutricion2);
        tvNutricion3 = (TextView) findViewById(R.id.tvNutricion3);
        tvNutricion4 = (TextView) findViewById(R.id.tvNutricion4);
        tvNutricion5 = (TextView) findViewById(R.id.tvNutricion5);


        Bundle extras = getIntent().getExtras();

        String d1 = extras.getString("dato1");

        String url = "https://api.edamam.com/search?q=" + d1 + "&app_id=4a0e061c&app_key=a761efa4d7cbd94cfdbadaa69ea25a9d";
        StringRequest solicitud = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // llega la respuesta del servidor
                        try {
                            Log.i("Respuesta", response);
                            JSONObject respuestaJSON = new JSONObject(response);

                            JSONArray nutriJSON = respuestaJSON.getJSONArray("hits");
                            JSONObject nutri2JSON = nutriJSON.getJSONObject(0);
                            JSONObject nutri3JSON = nutri2JSON.getJSONObject("recipe");
                            JSONObject nutri4JSON = nutri3JSON.getJSONObject("totalNutrients");



                            JSONObject nutri5JSON = nutri4JSON.getJSONObject("ENERC_KCAL");
                            int energia = nutri5JSON.getInt("quantity");

                            tvNutricion.setText("Energia = " + energia + " kcal");



                            JSONObject nutri6JSON = nutri4JSON.getJSONObject("FASAT");
                            int saturada = nutri6JSON.getInt("quantity");

                            tvNutricion1.setText("G. saturada = " + saturada + " g");



                            JSONObject nutri7JSON = nutri4JSON.getJSONObject("CHOCDF");
                            int carbohidrato = nutri7JSON.getInt("quantity");

                            tvNutricion2.setText("Carbohidrato = " + carbohidrato + " g");



                            JSONObject nutri8JSON = nutri4JSON.getJSONObject("SUGAR");
                            int azucar = nutri8JSON.getInt("quantity");



                            tvNutricion3.setText("Azúcar = " + azucar + " g");



                            JSONObject nutri9JSON = nutri4JSON.getJSONObject("PROCNT");
                            int proteinas = nutri9JSON.getInt("quantity");

                            tvNutricion4.setText("Proteína = " + proteinas + " g");



                            JSONObject nutri10JSON = nutri4JSON.getJSONObject("FIBTG");
                            int fibra = nutri10JSON.getInt("quantity");

                            tvNutricion5.setText("Fibra = " + fibra + " g");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
        listaEspera.add(solicitud);



    }



}
